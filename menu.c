#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define MAX_INPUT_SIZE 100

int main(int argc, char * argv[])
{
	char * img_output = calloc(MAX_INPUT_SIZE, sizeof(char));
	char * img_input = calloc(MAX_INPUT_SIZE, sizeof(char));
	char ** ops = calloc(1, sizeof(char *));
	char * tempstring = calloc(MAX_INPUT_SIZE, sizeof(char));
	char * poster_string = calloc(MAX_INPUT_SIZE, sizeof(char));
	int num_ops = 0;
	int menu_in;
	char poster_in;
	int menu_n_items;
	int x = 0;
	int ch;
	strcpy(img_output,"defaultOutput.png");
	strcpy(img_input,"defaultInput.pnm");
	do
	{
		printf("\nCurrent command:\n");
		printf("cat %s |", img_input);
		while(x < num_ops)
		{
			printf("%s", ops[x]);
			x++;
		}
		printf("convert - %s\n\n", img_output);
		x = 0;
	
		/* print menu */
		printf("What would you like to do?\n");
		printf("1. Specify input image\n");
		printf("--------------------------\n");
		printf("2. Negate (invert values)\n");
		printf("3. Posterise\n");
		printf("--------------------------\n");
		printf("4. Specify output image\n");
		printf("9. Reset command\n");
		printf("0. Exit\n");
		
		/* Getting input from user */
		menu_n_items = scanf("%d", &menu_in);
		while(ch=getc(stdin), ch != EOF && ch != '\n')
			;
		clearerr(stdin);
		printf("\n");
		if(menu_n_items == EOF)
		{
			fprintf(stderr, "Input failure.\n");
		}
		else if(menu_n_items == 0)
		{
			printf("Please enter a valid input\n");
		}		
		else if(menu_in == 1)
		{
			printf("Enter input image filename: ");
			fgets(tempstring, MAX_INPUT_SIZE+1, stdin);
			tempstring[strlen(tempstring) - 1] = '\0';
			printf("tempstring: %s\n", tempstring); 
			strcpy(img_input, tempstring);
		}
		else if(menu_in == 2)
		{
			num_ops++;
			ops = realloc(ops, num_ops*sizeof(char *));
			ops[num_ops - 1] = calloc(MAX_INPUT_SIZE, sizeof(char));
			strcpy(ops[num_ops - 1], " convert -negate - - | ");
		}
		else if(menu_in == 3)
		{
			num_ops++;
			ops = realloc(ops, num_ops*sizeof(char *));
			ops[num_ops - 1] = calloc(MAX_INPUT_SIZE, sizeof(char));
			printf("Enter number of posterise levels: ");
			scanf("%c", &poster_in);
			while(ch=getc(stdin), ch != EOF && ch != '\n')
				;
			clearerr(stdin);
			strcpy(poster_string, " convert -posterize ");
			strcat(poster_string, &poster_in);
			strcat(poster_string, " - - | ");
			strcpy(ops[num_ops - 1], poster_string);
		}
		else if(menu_in == 4)
		{
			printf("Enter output image filename: ");
			fgets(tempstring, MAX_INPUT_SIZE+1, stdin);
			tempstring[strlen(tempstring) - 1] = '\0';
			strcpy(img_output, tempstring);
		}
		else if(menu_in == 9)
		{
			while(x < num_ops)
			{
				free(ops[x]);
				x++;
			}
			free(ops);
			ops = calloc(1, sizeof(char *));
			strcpy(img_output,"defaultOutput.png");
			strcpy(img_input,"defaultInput.pnm");	
			num_ops = 0;
			x = 0;
		} 
	} while (menu_in != 0);
	if(num_ops != 0)
	{
		while(x < num_ops)
		{
			free(ops[x]);
			x++;
		}
		free(ops);
	}
	free(poster_string);
	free(tempstring);
	free(img_input);
	free(img_output);
	exit(1);
}
