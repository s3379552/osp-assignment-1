#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

int main(int argc, char * argv[])
{
	char * templine;
	int width;
	int height;
	int max;
	char * t_width = calloc(10, sizeof(char));
	char * t_height = calloc(10, sizeof(char));
	char * line_token = calloc(12, sizeof(char));
	char * t_max = calloc(10, sizeof(char));
	char * t_curr = calloc(10, sizeof(char));
	char * end_ptr;
	int poster_levels;
	int poster_areas;
	int poster_a_size;
	float curr_float;
	int curr_area;
	int line_flag = 0;
	int pixel_count = 0;
	int curr_pixel;
	int cm_flag = 0;
	int new_max = 0;

	if(argc != 2)
	{
		fprintf(stderr, "Not enough arguments given.\n");
		exit(0);
	}	

	/* get poster levels */
	poster_levels = (int) strtol(argv[1], &end_ptr, 10);
	poster_areas = 256 / poster_levels;
	poster_a_size = 255 / (poster_levels - 1);	

	/*get file type */
	templine = calloc(5, sizeof(char));
	while(!cm_flag)
	{
		if(fgets(templine, 6, stdin) == NULL)
		{
			fprintf(stderr, "Error reading file format.\n");
			exit(0);
		}
		else if(templine[0] == '#')
			continue;
		else
		{
			cm_flag = 1;
			if(templine[0] != 'P' && templine[1] != '2')
			{
				fprintf(stderr, "Incorrect file format.\n");
				exit(0);
			}
			else
			{
				fprintf(stderr, "File format accepted.\n");
			}
		}
	}
	templine[strlen(templine) - 1] = '\0';
	fprintf(stdout, "%s\n", templine);
	free(templine);
	cm_flag = 0;
	templine = calloc(20, sizeof(char));
	/*get dimensions*/
	while(!cm_flag)
	{	
		if(fgets(templine, 21, stdin) == NULL)
		{
			fprintf(stderr, "Error reading image dimensions.\n");
			exit(0);
		}
		else if(templine[0] == '#')
			continue;
		else
		{
			cm_flag = 1;
			templine[strlen(templine) - 1] = '\0';
			line_token = strtok(templine, " ");
			if(line_token == NULL)
			{
				fprintf(stderr, "Error reading width.\n");
				exit(0);
			}
			strcpy(t_width, line_token);
			width = (int) strtol(t_width, &end_ptr, 10);
			line_token = strtok(NULL, " ");
			if(line_token == NULL)
			{
				fprintf(stderr, "Error reading height.\n");
				exit(0);
			}
			strcpy(t_height, line_token);
			height = (int) strtol(t_height, &end_ptr, 10);
			fprintf(stderr, "WIDTH: %d\nHEIGHT: %d\n", width, height);
			line_token = strtok(NULL, " ");
		}
	}
	fprintf(stdout, "%d %d\n", width, height);
	free(templine);
	cm_flag = 0;
	/* get max value */
	templine = calloc(5, sizeof(char));
	while(!cm_flag)
	{
		if(fgets(templine, 6, stdin) == NULL)
		{
			fprintf(stderr, "Error reading max value.\n");
			exit(0);
		}
		else if(templine[0] == '#')
			continue;
		else
		{
			cm_flag = 1;
			templine[strlen(templine) - 1] = '\0'; 
			line_token = strtok(templine, " ");
			if(line_token == NULL)
			{
				fprintf(stderr, "Error reading max value.\n");
				exit(0);
			}
			strcpy(t_max, line_token);
			max = (int) strtol(t_max, &end_ptr, 10);
			fprintf(stderr, "MAX: %d\n", max);
		}
	}
	free(templine);
	templine = calloc(500, sizeof(char));
	fprintf(stdout, "255\n", max);
	cm_flag = 0;
	/* process image pixels */
	while(fgets(templine, 500, stdin) != NULL)
	{
		templine[strlen(templine) - 1] = '\0';
		line_flag = 0;
		line_token = strtok(templine, " ");
		if(line_token == NULL)
			line_flag = 1;
		else
		{
			strcpy(t_curr, line_token);
			curr_pixel = (int) strtol(t_curr, &end_ptr, 10);
			if(curr_pixel > max)
			{
				fprintf(stderr, "Pixel size exceeded.\n");
				exit(0);
			}
			pixel_count++;
			/* do operation on pixel */
			curr_float = ((float)curr_pixel/(float)poster_areas);
			curr_area = (int)(curr_float + 0.5);
			if(curr_area > curr_float)
				curr_area = curr_area - 1;
			curr_float = (float)poster_a_size * (float)curr_area;
			curr_area = (int)(curr_float + 0.5);
			if(curr_area > curr_float)
				curr_area = curr_area - 1;
			/*if(new pixel value > new_max)
 				new_max = new pixel value;*/
			fprintf(stdout, "%d ", curr_area);

		}
		while(!line_flag)
		{
			line_token = strtok(NULL, " ");
			if(line_token == NULL)
				line_flag = 1;
			else
			{
				strcpy(t_curr, line_token);
				curr_pixel = (int) strtol(t_curr, &end_ptr, 10);
				if(curr_pixel > max)
				{
					fprintf(stderr, "Pixel size exceeded.\n");
					exit(0);
				}
				pixel_count++;
				/* do operation on pixel */	
				curr_float = ((float)curr_pixel/(float)poster_areas);
				curr_area = (int)(curr_float + 0.5);
				if(curr_area > curr_float)
					curr_area = curr_area - 1;
				curr_float = (float)poster_a_size * (float)curr_area;
				curr_area = (int)(curr_float + 0.5);
				if(curr_area > curr_float)
					curr_area = curr_area - 1;				

				/* if(new pixel value > new_max)
 					new_max = new pixel value;*/
				fprintf(stdout, "%d ", curr_area);
			}
		}
		fprintf(stdout, "\n");
	}
	fprintf(stderr, "PIXEL COUNT: %d\nHEIGHT*WIDTH: %d\n", pixel_count, (height*width));
	if(pixel_count != (height*width))
	{
		fprintf(stderr, "Specified pixel count does not match image height\n");
		exit(0);
	}

	free(templine);
	free(t_max);
	free(t_height);
	free(t_width);
	free(t_curr);
	free(line_token);
	exit(1);
}
